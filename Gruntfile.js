module.exports = function(grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON("package.json"),
    concat: {
      dist: {
        src: [ "src/*.scss" ],
        dest: "dist/<%= pkg.name %>.scss"
      }
    },
    sass: {
      test: {
        options: { sourcemap: "none" },
        files: [ { ".sass-test/test.css": "test/test.scss" } ]
      },
    },
    watch: {
      sass: {
        files: [ "src/*.scss" ],
        tasks: [ "concat:dist", "sass:test" ]
      },
      test: {
        files: [ "test/*.scss" ],
        tasks: [ "sass:test" ]
      }
    },
  });
  grunt.loadNpmTasks("grunt-contrib-concat");
  grunt.loadNpmTasks("grunt-contrib-sass");
  grunt.loadNpmTasks("grunt-contrib-watch");
  grunt.registerTask("default", ["watch"]);
}